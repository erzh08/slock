# My build of slock

## Patches

- [Foreground and Background](https://tools.suckless.org/slock/patches/foreground-and-background/): Locks with a blurred screenshot as backround and dwm logo on foreground.
- [Message](https://tools.suckless.org/slock/patches/message/): Makes it possible to use a message (on config.h or using -m) and a font.
- [Terminal Keys](https://tools.suckless.org/slock/patches/terminalkeys/): Adds common terminal keys (Like ^u ou ^h) to slock.
- [Control Clear](https://tools.suckless.org/slock/patches/control-clear/): Makes it possible to no longer change to failure color if a conrol key is pressed while the buffer is empty.


## Requirements

In order to build slock you need the Xlib header files.

## Installation

```bash
git clone https://gitlab.com/erzh08/slock.git
cd slock
sudo make install
```
